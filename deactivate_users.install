<?php

/**
 * @file
 * Update hooks for deactivate_users module.
 */

use Drupal\Core\Utility\UpdateException;

/**
 * Implements hook_update_N().
 *
 * Add new subject default settings.
 */
function deactivate_users_update_8001(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config_factory->getEditable('deactivate_users.settings')
    ->set('notify_email.enabled', 1)
    ->set('notify_email.subject', '[site:name] Account Expiring Soon')
    ->set('deactivated_email.enabled', 1)
    ->set('deactivated_email.from_address', '')
    ->set('deactivated_email.subject', '[site:name] Account Expired')
    ->set('deactivated_email.message', 'Your account has been deactivated for inactivity.')
    ->set('timeout.changed_record', 86400)
    ->save(TRUE);
}

/**
 * Implements hook_update_N().
 *
 * Add default minimum warning time setting.
 */
function deactivate_users_update_8002(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config_factory->getEditable('deactivate_users.settings')
    ->set('minimum_warning_time_days', 0)
    ->save(TRUE);
}

/**
 * Implements hook_update_N().
 *
 * Add log_notifications default setting.
 */
function deactivate_users_update_8003(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config_factory->getEditable('deactivate_users.settings')
    ->set('log_notifications', 1)
    ->save();
}

/**
 * Implements hook_update_N().
 *
 * Add the AccountStatusRecord entity type and install the config for the
 * user unblock portion of the module.
 */
function deactivate_users_update_8004(): string {
  $config = \Drupal::service('config.factory')->getEditable('deactivate_users.settings');
  $config->set('enable_unblock', 0)
    ->set('unblock_email.from_address', '')
    ->set('unblock_email.subject', '[site:name] Unblock Your Account')
    ->set('unblock_email.message', 'To unblock your account, go to [user:unblock-link]')
    ->set('timeout.unblock_email', '86400')
    ->save();

  $changeList = \Drupal::entityDefinitionUpdateManager()->getChangeList();
  if (!array_key_exists('account_status_record', $changeList)) {
    return 'account_status_record entity type is already installed.';
  }
  try {
    $accountStatusRecordEntityTypeDefinition = \Drupal::service('entity_type.manager')->getDefinition('account_status_record');
    \Drupal::entityDefinitionUpdateManager()->installEntityType($accountStatusRecordEntityTypeDefinition);
  }
  catch (\Exception $exception) {
    throw new UpdateException($exception->getMessage());
  }

  return 'Installed the new content entity type account_status_record and set user unblock config.';
}
