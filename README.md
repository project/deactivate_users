# Deactivate Inactive Users
Drupal module to block accounts that have been inactive for a number of days.
