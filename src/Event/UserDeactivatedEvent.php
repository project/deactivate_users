<?php

namespace Drupal\deactivate_users\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Event that is fired when a user is deactivated.
 */
class UserDeactivatedEvent extends Event {

  const EVENT_NAME = 'deactivate_users_user_deactivated';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the deactivated user.
   */
  public function __construct(UserInterface $account) {
    $this->account = $account;
  }

}
