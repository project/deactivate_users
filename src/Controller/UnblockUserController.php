<?php

namespace Drupal\deactivate_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * A controller for users to unblock their emails with a link.
 */
class UnblockUserController extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userData;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritDoc}
   */
  public function __construct(UserStorageInterface $userData, RendererInterface $renderer) {
    $this->userData = $userData;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): UnblockUserController {
    return new static(
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('renderer')
    );
  }

  /**
   * Returns a render-able array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function content(string $uid, string $timestamp, string $hash): array|RedirectResponse {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->userData->load($uid);
    // Get the config timeout for unblocking an account.
    $config = $this->config('deactivate_users.settings');
    $unblock_timeout = $config->get('timeout.unblock_email');
    // Check to see if the hash is valid and hasn't timed out.
    if ($user && $hash === user_pass_rehash($user, $timestamp) && time() - $unblock_timeout <= $timestamp) {
      // If the hash is good, unblock the user and send them to the login page.
      if ($user->isBlocked()) {
        $user->activate()->save();
        $this->messenger()->addMessage($this->t('Your account has been unblocked.'));
        return $this->redirect('user.login');
      }
      // Otherwise, if the user is already unblocked, say so.
      $build = [
        '#markup' => $this->t('This account has already been unblocked. Please try to login again.'),
      ];
    }
    // If the hash is no good or has timed out, tell the user.
    else {
      $return_link = Link::createFromRoute($this->t('RETURN TO THE UNBLOCK PAGE'), 'deactivate_users.unblock.generate')->toRenderable();
      $return_link['#attributes']['class'][] = 'button';
      $build = [
        '#markup' => '<p>' . $this->t("That unblock link either doesn't exist or has expired. If you wish to still unblock your account, please return to the unblock page and resubmit your email address.") . '</p>' . $this->renderer->render($return_link),
      ];
    }

    return $build;
  }

}
