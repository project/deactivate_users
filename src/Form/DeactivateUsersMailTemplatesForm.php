<?php

namespace Drupal\deactivate_users\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure user account deactivation settings for this site.
 */
class DeactivateUsersMailTemplatesForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'deactivate_users.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deactivate_users_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['notify_email'] = [
      '#type' => 'details',
      '#title' => $this->t('Warning Notification Email'),
      '#open' => FALSE,
    ];

    $form['notify_email']['notify_email_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable warning notification email'),
      '#default_value' => $config->get('notify_email.enabled'),
    ];

    // Return address.
    $form['notify_email']['notify_email_from_address'] = [
      '#type' => 'email',
      '#title' => $this->t('From address'),
      '#default_value' => $config->get('notify_email.from_address'),
      '#description' => $this->t('The address the user will see the email as coming from; if left empty, defaults to site-wide email address.'),
      '#required' => FALSE,
    ];

    $form['notify_email']['notify_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
      '#default_value' => $config->get('notify_email.subject'),
      '#description' => $this->t('The subject line for the email.  Supports tokens.'),
      '#required' => TRUE,
    ];

    // Message to send to user.
    $form['notify_email']['notify_email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user'),
      '#default_value' => $config->get('notify_email.message'),
      '#description' => $this->t('Use the token [user:expire-timeout:days] the number of days remaining. Supports tokens.'),
      '#required' => TRUE,
    ];

    $form['deactivated_email'] = [
      '#type' => 'details',
      '#title' => $this->t('Deactivated Account Email'),
      '#open' => FALSE,
    ];

    $form['deactivated_email']['deactivated_email_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable deactivated account email'),
      '#default_value' => $config->get('deactivated_email.enabled'),
    ];
    // Return address.
    $form['deactivated_email']['deactivated_email_from_address'] = [
      '#type' => 'email',
      '#title' => $this->t('From address'),
      '#default_value' => $config->get('deactivated_email.from_address'),
      '#description' => $this->t('The address the user will see the email as coming from; if left empty, defaults to site-wide email address.'),
      '#required' => FALSE,
    ];

    $form['deactivated_email']['deactivated_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
      '#default_value' => $config->get('deactivated_email.subject'),
      '#description' => $this->t('The subject line for the email.  Supports tokens.'),
      '#required' => TRUE,
    ];

    // Message to send to user.
    $form['deactivated_email']['deactivated_email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user'),
      '#default_value' => $config->get('deactivated_email.message'),
      '#description' => $this->t('Use token [user:expire-timeout:days] for the number of days remaining. Supports tokens.'),
      '#required' => TRUE,
    ];

    $form['unblock_email'] = [
      '#type' => 'details',
      '#title' => $this->t('Unblock User Email'),
      '#open' => FALSE,
    ];

    // Return address.
    $form['unblock_email']['unblock_email_from_address'] = [
      '#type' => 'email',
      '#title' => $this->t('From address'),
      '#default_value' => $config->get('unblock_email.from_address'),
      '#description' => $this->t('The address the user will see the email as coming from; if left empty, defaults to site-wide email address.'),
      '#required' => FALSE,
    ];

    // Subject of email.
    $form['unblock_email']['unblock_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
      '#default_value' => $config->get('unblock_email.subject'),
      '#description' => $this->t('The subject line for the email.  Supports tokens.'),
      '#required' => TRUE,
    ];

    // Message to send to user.
    $form['unblock_email']['unblock_email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to user'),
      '#default_value' => $config->get('unblock_email.message'),
      '#description' => $this->t('Use token [user:unblock-link] for the link to unblock their account. Supports tokens.'),
      '#required' => TRUE,
    ];

    // Add a token browser.
    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['random', 'language', 'current-date', 'site', 'user'],
      '#global_types' => FALSE,
      '#show_nested' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate Email, if it's been entered.
    $email_address = $form_state->getValue('notify_email_from_address');
    if (($email_address === "0" || $email_address) && !filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName('notify_email_from_address', $this->t('Please enter a valid email address.'));
    }
    $email_address = $form_state->getValue('deactivated_email_from_address');
    if (($email_address === "0" || $email_address) && !filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName('deactivated_email_from_address', $this->t('Please enter a valid email address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('notify_email.enabled', $form_state->getValue('notify_email_enabled'))
      ->set('notify_email.from_address', $form_state->getValue('notify_email_from_address'))
      ->set('notify_email.subject', $form_state->getValue('notify_email_subject'))
      ->set('notify_email.message', $form_state->getValue('notify_email_message'))
      ->set('deactivated_email.enabled', $form_state->getValue('deactivated_email_enabled'))
      ->set('deactivated_email.from_address', $form_state->getValue('deactivated_email_from_address'))
      ->set('deactivated_email.subject', $form_state->getValue('deactivated_email_subject'))
      ->set('deactivated_email.message', $form_state->getValue('deactivated_email_message'))
      ->set('unblock_email.from_address', $form_state->getValue('unblock_email_from_address'))
      ->set('unblock_email.subject', $form_state->getValue('unblock_email_subject'))
      ->set('unblock_email.message', $form_state->getValue('unblock_email_message'))
      ->save();
    // Submit form.
    parent::submitForm($form, $form_state);
  }

}
