<?php

namespace Drupal\deactivate_users\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure user account deactivation settings for this site.
 */
class DeactivateUsersSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'deactivate_users.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deactivate_users_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on cron'),
      '#default_value' => $config->get('enabled'),
      '#description' => $this->t('Enable deactivating users when cron runs.'),
    ];

    $form['log_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log notifications'),
      '#default_value' => $config->get('log_notifications'),
      '#description' => $this->t('Log sent notifications to Watchdog.'),
    ];

    $form['enable_unblock'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable User Unblock'),
      '#default_value' => $config->get('enable_unblock'),
      '#description' => $this->t("Allow users to unblock themselves if they've been blocked due to inactivity."),
    ];

    $form['minimum_warning_time_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum number of days to warn users before blocking.'),
      '#default_value' => $config->get('minimum_warning_time_days'),
      '#description' => $this->t('Number of days to warn users after first installing the module before blocking them.'),
    ];

    // Number of days to deactivate user account.
    $form['timeout_inactive'] = [
      '#type' => 'number',
      '#title' => $this->t('Inactivity limit before deactivation (days)'),
      '#default_value' => $config->get('timeout.inactive'),
      '#description' => $this->t('Time (in days) until user account is deactivated.'),
      '#required' => TRUE,
    ];

    // Grace period in days to add to inactive time.
    $form['timeout_grace_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Grace period (days)'),
      '#default_value' => $config->get('timeout.grace_period'),
      '#description' => $this->t('Number of days to wait after the Inactivity Limit before users are actually blocked.'),
      '#required' => FALSE,
    ];

    // List of days to notify user via email.
    $form['notify_email_days'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Warning notification schedule (days)'),
      '#default_value' => $config->get('notify_email.days'),
      '#description' => $this->t('How many days in advance of the Inactivity Limit should the system send the user a notice. Comma-separated list of integers. <em>Example: 5, 10, 25</em>'),
      '#size' => 30,
      '#required' => FALSE,
    ];

    $form['timeout_changed_record'] = [
      '#type' => 'number',
      '#title' => $this->t('Changed Record Grace Period (seconds)'),
      '#default_value' => $config->get('timeout.changed_record'),
      '#description' => $this->t('Do not block users who have changed in the last <em>n</em> seconds, so cron does not immediately re-block a user the admin just unblocked.'),
      '#required' => TRUE,
    ];

    $form['timeout_unblock_email'] = [
      '#type' => 'number',
      '#title' => $this->t('Unblock Email Grace Period (seconds)'),
      '#default_value' => $config->get('timeout.unblock_email'),
      '#description' => $this->t("If User Unblock is enabled, the user will have <em>n</em> seconds to use the link that's emailed to them before it expires."),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate Days list.
    if (!preg_match('/^\d+(?:,\d+)*$/', str_replace(' ', '', $form_state->getValue('notify_email_days')))) {
      $form_state->setErrorByName('notify_email_days', $this->t('Please enter a comma-separated list of numbers only.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('log_notifications', $form_state->getValue('log_notifications'))
      ->set('enable_unblock', $form_state->getValue('enable_unblock'))
      ->set('minimum_warning_time_days', $form_state->getValue('minimum_warning_time_days'))
      ->set('notify_email.days', $form_state->getValue('notify_email_days'))
      ->set('timeout.inactive', $form_state->getValue('timeout_inactive'))
      ->set('timeout.grace_period', $form_state->getValue('timeout_grace_period'))
      ->set('timeout.changed_record', $form_state->getValue('timeout_changed_record'))
      ->set('timeout.unblock_email', $form_state->getValue('timeout_unblock_email'))
      ->save();
    // Submit form.
    parent::submitForm($form, $form_state);
  }

}
