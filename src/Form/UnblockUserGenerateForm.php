<?php

namespace Drupal\deactivate_users\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller for generating user unblock emails.
 */
class UnblockUserGenerateForm extends FormBase {

  /**
   * MailManager service object.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Messenger service object.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor for a new UnblockUserGenerateForm class.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   MailManager service object.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service object.
   */
  public function __construct(MailManagerInterface $mailManager, MessengerInterface $messenger) {
    $this->mailManager = $mailManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'deactivated_users_unblock_generate';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#markup' => $this->t('Enter your email address associated with the site. If the email exists, an email will be sent with a link to click on to unblock your account.'),
    ];
    $form['user_email'] = [
      '#type' => 'email',
      '#title' => 'Email address',
      '#description' => $this->t('Enter your email address associated with the site.'),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $user_email = $form_state->getValue('user_email');

    /** @var \Drupal\user\Entity\User $user */
    $user = user_load_by_mail($user_email);
    if ($user && $user->isBlocked()) {
      $this->mailManager->mail(
        'deactivate_users',
        'unblock_email', $user->getEmail(),
        $user->getPreferredLangcode(),
        ['uid' => $user->id()]
      );
      $this->getLogger('deactivate_users')->notice(
        'Sent user @username (@mail) an unblock email.',
        ['@username' => $user->getAccountName(), '@mail' => $user->getEmail()]
      );
    }

    $this->messenger->addMessage('If that email address exists in the system, an email will be sent with further instructions. Please check Spam or Quarantine folders if necessary.');
  }

  /**
   * Only allows access to unblock functionality if it's enabled.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(): AccessResultInterface {
    // Get the config setting for user unblock, and return its value.
    $config = $this->configFactory()->get('deactivate_users.settings');
    $unblock_enabled = $config->get('enable_unblock');
    return AccessResult::allowedIf($unblock_enabled);
  }

}
