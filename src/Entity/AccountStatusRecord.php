<?php

namespace Drupal\deactivate_users\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Block Record entity.
 *
 * @ingroup user_deactivate
 *
 * @ContentEntityType(
 *   id = "account_status_record",
 *   label = @Translation("Account Status Record"),
 *   base_table = "account_status_record",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "date" = "date",
 *     "action" = "action",
 *     "method" = "method",
 *     "description" = "description",
 *   },
 * )
 */
class AccountStatusRecord extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Block Record entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Account Status Record entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['uid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('UID'))
      ->setDescription(t('The UID of the user that had their account blocked/activated.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['date'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Date'))
      ->setDescription(t('The Date of the action.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['action'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action'))
      ->setDescription(t('Whether the account was blocked or unblocked.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('method'))
      ->setDescription(t('The method by which the user was blocked/activated; i.e. by an admin or the system.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['by_uid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('By UID'))
      ->setDescription(t('The UID of the user that blocked/activated the account, if it was a user that did it.'))
      ->setReadOnly(TRUE);
    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UUID'))
      ->setDescription(t('Any description supplied by the user doing the blocking, or from the system.'))
      ->setReadOnly(TRUE);

    return $fields;
  }

  /**
   * Return the method used to block the user; by system or by user.
   *
   * @return mixed
   *   How the user was disabled, by user or system.
   */
  public function getMethod() {
    return $this->get('method')->getValue()[0]['value'];
  }

}
