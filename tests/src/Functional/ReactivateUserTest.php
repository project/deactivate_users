<?php

namespace Drupal\Tests\deactivate_users\Functional;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\deactivate_users\Traits\TestLoggerTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use GuzzleHttp\Cookie\CookieJar;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Ensure that account reactivation method works as expected.
 *
 * @group deactivate_users
 */
class ReactivateUserTest extends BrowserTestBase {
  use TestLoggerTrait;
  use AssertMailTrait {
    getMails as drupalGetMails;
  }

  /**
   * The user object to test password resetting.
   *
   * @var \Drupal\user\Entity\UserInterface
   */
  protected UserInterface $account;

  /**
   * Language manager object.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['deactivate_users', 'token', 'user', 'dblog'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The cookie jar.
   *
   * @var \GuzzleHttp\Cookie\CookieJar
   */
  protected $cookies;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->container->get('config.factory')
      ->getEditable('deactivate_users.settings')
      ->set('deactivated_email.enabled', 1)
      ->set('notify_email.enabled', 1)
      ->set('notify_email.days', '30')
      ->set('timeout.inactive', 90)
      ->set('timeout.grace_period', 7)
      ->set('minimum_warning_time_days', 15)
      ->set('log_notifications', 1)
      // Set the changed record threshold timestamp to 1 hour in the future to
      // ensure all users are considered.
      ->set('timeout.changed_record', -3600)
      ->set('enabled', 1)
      ->save(TRUE);
    $this->cookies = new CookieJar();
    $encoders = [new JsonEncoder(), new XmlEncoder()];
    $this->serializer = new Serializer([], $encoders);
  }

  /**
   * Tests password reset functionality.
   */
  public function testUserSelfUnblock() {
    // Grab the current timestamp.
    $now = time();

    // Inactive user created 180 days ago,
    // last login 100 days ago.
    $testUser = $this->drupalCreateUser();
    $testUser->created = $now - 86400 * 180;
    $testUser->setLastAccessTime($now - 86400 * 100)
      ->activate()
      ->save();

    // Active user created 180 days ago,
    // last login 10 days ago.
    $activeUser = $this->drupalCreateUser();
    $activeUser->created = $now - 86400 * 180;
    $activeUser->setLastAccessTime($now - 86400 * 10)
      ->activate()
      ->save();

    // Manually blocked user created 10 days ago,
    // last login 1 day ago.
    $blockedUser = $this->drupalCreateUser();
    $blockedUser->created = $now - 86400 * 10;
    $blockedUser->setLastAccessTime($now - 86400 * 1)
      ->block()
      ->save();

    // Run deactivate_users_cron(), pretending we started running this 1yr ago
    // so minimum times don't apply.
    // This should deactivate testUser.
    \Drupal::state()->set('deactivate_users.first_sent_timestamp', $now - 365 * 86400);
    \Drupal::state()->set('deactivate_users.last_emails_sent_timestamps', []);
    deactivate_users_cron();

    // Check that the testUser was blocked by the system for inactivity.
    $result = \Drupal::entityQuery('account_status_record')
      ->accessCheck(FALSE)
      ->condition('uid', $testUser->id())
      ->sort('id', 'DESC')
      ->range(0, 1)
      ->execute();
    $active_status_record_id = reset($result);
    $storage = \Drupal::entityTypeManager()->getStorage('account_status_record');
    $asr = $storage->load((int) $active_status_record_id);
    $this->assertNotNull($asr, 'Account Status Record found for testUser');
    $this->assertSame($asr->getMethod(), 'by system', 'User was blocked by system.');

    // Don't send emails to not-a-user.
    $this->accountUnblockRequest(['edit-user-email' => 'not-a-user@example.org']);
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Sent user @username (@mail) an unblock email.', [
      '@username' => 'not-a-user',
      '@mail' => 'not-a-user@example.org',
    ]);

    // Active users should not get unblock notices, either.
    $this->assertFalse(User::load($activeUser->id())->isBlocked());
    $this->accountUnblockRequest(['edit-user-email' => $activeUser->getEmail()]);
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Sent user @username (@mail) an unblock email.', [
      '@username' => $activeUser->getAccountName(),
      '@mail' => $activeUser->getEmail(),
    ]);

    // Make sure the testUser is blocked before trying to unblock them.
    $response = $this->loginRequest($testUser->getAccountName(), $testUser->passRaw);
    $this->assertEquals(400, $response->getStatusCode(), 'Test User is blocked.');

    // The blockedUser is blocked in code (see above), they should be unblocked.
    $this->accountUnblockRequest(['edit-user-email' => $blockedUser->getEmail()]);
    $this->assertLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Sent user @username (@mail) an unblock email.', [
      '@username' => $blockedUser->getAccountName(),
      '@mail' => $blockedUser->getEmail(),
    ]);
    $this->accountUnblockFromEmail();

    // The test user was blocked by the deactivate_users_cron() call above.
    $this->accountUnblockRequest(['edit-user-email' => $testUser->getEmail()]);
    $this->assertLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Sent user @username (@mail) an unblock email.', [
      '@username' => $testUser->getAccountName(),
      '@mail' => $testUser->getEmail(),
    ]);
    $this->accountUnblockFromEmail();
  }

  /**
   * Unblock a user's account from the link in the email.
   */
  public function accountUnblockFromEmail() {
    // Assume the most recent email.
    $_emails = $this->drupalGetMails();
    $email = end($_emails);
    $urls = [];
    preg_match('#.+user/unblock/.+#', $email['body'], $urls);
    $resetURL = $urls[0];
    $this->drupalGet($resetURL);
    $this->assertSession()->pageTextContains('Your account has been unblocked.');
  }

  /**
   * Execute an account reset request.
   *
   * @param array $form_fields
   *   The data in the form to submit.
   */
  public function accountUnblockRequest(array $form_fields = []) {
    $password_reset_url = Url::fromRoute('deactivate_users.unblock.generate')
      ->setAbsolute()
      ->toString();
    $this->drupalGet($password_reset_url);
    $this->submitForm($form_fields, 'Submit');
  }

  /**
   * Executes a password HTTP request for a given serialization format.
   *
   * @param array $request_body
   *   The request body.
   * @param string $format
   *   The format to use to make the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  public function accountUnblockRequestFormat(array $request_body, $format = 'json') {
    $password_reset_url = Url::fromRoute('deactivate_users.unblock.http')
      ->setRouteParameter('_format', $format)
      ->setAbsolute();

    $result = \Drupal::httpClient()->post($password_reset_url->toString(), [
      'body' => $this->serializer->encode($request_body, $format),
      'headers' => [
        'Accept' => "application/$format",
      ],
      'http_errors' => FALSE,
      'cookies' => $this->cookies,
    ]);

    return $result;
  }

  /**
   * Executes a login HTTP request for a given serialization format.
   *
   * @param string $name
   *   The username.
   * @param string $pass
   *   The user password.
   * @param string $format
   *   The format to use to make the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  protected function loginRequest($name, $pass, $format = 'json') {
    $user_login_url = Url::fromRoute('user.login.http')
      ->setRouteParameter('_format', $format)
      ->setAbsolute();

    $request_body = [];
    if (isset($name)) {
      $request_body['name'] = $name;
    }
    if (isset($pass)) {
      $request_body['pass'] = $pass;
    }

    $result = \Drupal::httpClient()->post($user_login_url->toString(), [
      'body' => $this->serializer->encode($request_body, $format),
      'headers' => [
        'Accept' => "application/$format",
      ],
      'http_errors' => FALSE,
      'cookies' => $this->cookies,
    ]);
    return $result;
  }

}
