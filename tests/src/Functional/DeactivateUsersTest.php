<?php

namespace Drupal\Tests\deactivate_users\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\deactivate_users\Traits\TestLoggerTrait;
use Drupal\user\Entity\User;

/**
 * Test that users beyond the threshold are deactivated as intended.
 *
 * @group deactivate_users
 */
class DeactivateUsersTest extends BrowserTestBase {
  use TestLoggerTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['deactivate_users', 'token', 'user', 'dblog'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->container->get('config.factory')
      ->getEditable('deactivate_users.settings')
      ->set('deactivated_email.enabled', 1)
      ->set('notify_email.enabled', 1)
      ->set('notify_email.days', '30')
      ->set('timeout.inactive', 90)
      ->set('timeout.grace_period', 7)
      ->set('minimum_warning_time_days', 15)
      ->set('log_notifications', 1)
      // Set the changed record threshold timestamp to 1 hour in the future to
      // ensure all users are considered.
      ->set('timeout.changed_record', -3600)
      ->set('enabled', 1)
      ->save(TRUE);
  }

  /**
   * Test users are expired as expected.
   */
  public function testExpired() {

    // Grab the current timestamp.
    $now = time();

    // Active user created 180 days ago,
    // last login 85 days ago.
    // Will be notified, but not blocked.
    $active_user = $this->drupalCreateUser();
    $active_user->created = $now - 86400 * 180;
    $active_user->setLastAccessTime($now - 86400 * 85)
      ->activate()
      ->save();

    // Inactive user created 180 days ago,
    // last login 100 days ago.
    $inactive_user = $this->drupalCreateUser();
    $inactive_user->created = $now - 86400 * 180;
    $inactive_user->setLastAccessTime($now - 86400 * 100)
      ->activate()
      ->save();

    // Never logged in user created 180 days ago,
    // no login timestamp, no access timestamp.
    $no_login_user = $this->drupalCreateUser();
    $no_login_user->created = $now - 86400 * 180;
    $no_login_user->setLastLoginTime($no_login_user->access = 0)
      ->activate()
      ->save();

    // Never logged in user created 20 days ago,
    // no login timestamp, no access timestamp.
    $new_no_login_user = $this->drupalCreateUser();
    $new_no_login_user->created = $now - 86400 * 20;
    $new_no_login_user->setLastLoginTime($no_login_user->access = 0)
      ->activate()
      ->save();

    $blocked_user = $this->drupalCreateUser();
    $blocked_user
      ->block()
      ->save();

    // Run deactivate_users_cron() pretending we've only just started sending
    // out warnings.
    \Drupal::state()->set('deactivate_users.first_sent_timestamp', $now - 3600);
    \Drupal::state()->set('deactivate_users.last_emails_sent_timestamps', []);

    // Check all tokens are 14 days, because that is the minimum we will return.
    $this->assertEquals('14', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $active_user]), 'Active user: expires in (min) 14 days.');
    $this->assertEquals('14', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $inactive_user]), 'Inactive user: expires in (min) 14 days.');
    $this->assertEquals('14', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $no_login_user]), 'Never logged in: expires in (min) 14 days.');
    $this->assertEquals('70', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $new_no_login_user]), 'New never logged in, 20 days-old: expires in 70 days.');

    deactivate_users_cron();

    // Check users are not deactivated while inside the minimum time.
    $this->assertFalse(User::load($active_user->id())->isBlocked(), 'Active user is active, inside minimum time.');
    $this->assertFalse(User::load($inactive_user->id())->isBlocked(), 'Inactive user warned while inside minimum time.');
    $this->assertFalse(User::load($no_login_user->id())->isBlocked(), 'Never logged in user is not blocked while inside minimum time.');
    $this->assertFalse(User::load($new_no_login_user->id())->isBlocked(), 'New never logged in user is active, inside minimum time.');
    $this->assertTrue(User::load($blocked_user->id())->isBlocked(), 'Blocked users remain blocked.');

    // Check for log messages about pending deactivations.
    $this->assertLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $active_user->getAccountName(),
      '@mail' => $active_user->getEmail(),
    ]);
    // These two would be blocked, but get a minimum time deactivation notice.
    $this->assertLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $inactive_user->getAccountName(),
      '@mail' => $inactive_user->getEmail(),
    ]);
    // Users who have never logged in are not notified of pending deactivations.
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $no_login_user->getAccountName(),
      '@mail' => $no_login_user->getEmail(),
    ]);
    // User is only 20 days old, not old enough to notify.
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $new_no_login_user->getAccountName(),
      '@mail' => $new_no_login_user->getEmail(),
    ]);

    // Reset the logger and state for a second run.
    $this->resetLogs();

    // Run deactivate_users_cron(), pretending we started running this 1yr ago
    // so minimum times don't apply.
    \Drupal::state()->set('deactivate_users.first_sent_timestamp', $now - 365 * 86400);
    \Drupal::state()->set('deactivate_users.last_emails_sent_timestamps', []);

    // Do a second test.
    deactivate_users_cron();

    // Check tokens.
    $this->assertEquals('5', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $active_user]), 'Active user 5 days.');
    $this->assertEquals('0', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $inactive_user]), 'Inactive user 0 days.');
    $this->assertEquals('0', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $no_login_user]), 'Never logged in user 0 days.');
    $this->assertEquals('70', \Drupal::token()->replace('[user:expire-timeout:days]', ['user' => $new_no_login_user]), 'New user never logged in, 70 days.');

    // Again, check the users are deactivated as expected.
    $this->assertFalse(User::load($active_user->id())->isBlocked(), 'Active user is active');
    $this->assertTrue(User::load($inactive_user->id())->isBlocked(), 'Inactive user blocked.');
    $this->assertTrue(User::load($no_login_user->id())->isBlocked(), 'Never logged in user blocked.');
    $this->assertFalse(User::load($new_no_login_user->id())->isBlocked(), 'New never logged in user is active.');
    $this->assertTrue(User::load($blocked_user->id())->isBlocked(), 'Blocked users still remain blocked.');

    // Check logs.
    $this->assertLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $active_user->getAccountName(),
      '@mail' => $active_user->getEmail(),
    ]);
    // These two are blocked, and should not get pending deactivation messages.
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $inactive_user->getAccountName(),
      '@mail' => $inactive_user->getEmail(),
    ]);
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $no_login_user->getAccountName(),
      '@mail' => $no_login_user->getEmail(),
    ]);
    // User is only 20 days old, not old enough to notify.
    $this->assertNoLogExists('deactivate_users', RfcLogLevel::NOTICE, 'Notified @username (@mail) of pending deactivation.', [
      '@username' => $new_no_login_user->getAccountName(),
      '@mail' => $new_no_login_user->getEmail(),
    ]);
  }

}
