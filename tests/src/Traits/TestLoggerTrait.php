<?php

namespace Drupal\Tests\deactivate_users\Traits;

/**
 * A trait to find log messages in the watchdog table.
 */
trait TestLoggerTrait {

  /**
   * The last time resetLogs() was called.
   *
   * @var int
   */
  protected $logResetTimestamp = 0;

  /**
   * Assert that a log message exists in the logger.
   *
   * @param string $channel
   *   The channel where the message was logged.
   * @param string $level
   *   The severity level of the log message.  e.g, 'notice' or 'error'.
   * @param string $message
   *   The message.
   * @param array $context
   *   The context passed in with the message.
   */
  protected function assertLogExists($channel, $level, string $message, array $context = []) {
    $this->assertTrue($this->hasLogMessage($channel, $level, $message, $context));
  }

  /**
   * Assert that a log message doesn't exist in the logger.
   *
   * @param string $channel
   *   The channel where the message was logged.
   * @param string $level
   *   The severity level of the log message.  e.g, 'notice' or 'error'.
   * @param string $message
   *   The message.
   * @param array $context
   *   The context passed in with the message.
   */
  protected function assertNoLogExists($channel, $level, string $message, array $context = []) {
    $this->assertFalse($this->hasLogMessage($channel, $level, $message, $context));
  }

  /**
   * Check if a message exists in the watchdog logs.
   *
   * This function looks for the specified message in the watchdog logs since
   * the last call to TestLoggerTrait::resetLogs(). It can take the same
   * arguments that the logging service takes to make it possible to search for
   * log messages based on ephemeral data, such as a test user's email.
   *
   * @param string $channel
   *   The log channel.
   * @param int $level
   *   The RFC Log level.
   * @param string $raw_message
   *   The message to search for.
   * @param array $context
   *   The message context, used to fill in variables in the message.
   *
   * @return bool
   *   Return TRUE if the message is found, FALSE otherwise.
   */
  protected function hasLogMessage($channel, $level, $raw_message, $context = []): bool {
    $message = strtr($raw_message, $context);
    $records = \Drupal::database()->select('watchdog', 'w')
      ->fields('w', ['type', 'message', 'variables', 'severity', 'timestamp'])
      ->condition('w.type', $channel)
      ->condition('w.severity', $level)
      ->condition('w.timestamp', $this->logResetTimestamp, '>')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($records as $rec) {
      $formatted = strtr($rec['message'], unserialize($rec['variables'], ['allowed_classes' => FALSE]));
      if (strpos($formatted, $message) !== FALSE) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Reset the logs.
   *
   * Reset the limit of how far back the trait logs. Use this when you have run
   * a test sequence that will create a valid log that should no longer be
   * considered.
   */
  protected function resetLogs() {
    $this->logResetTimestamp = time();
    sleep(1);
  }

}
